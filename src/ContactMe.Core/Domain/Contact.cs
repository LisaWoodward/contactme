using System;
using System.Collections.Generic;
using ContactMe.Core.Services;

namespace ContactMe.Core.Domain
{
    public class Contact
    {
        public virtual  Guid Id { get; set; }

        public virtual DateTime CreatedDate { get; set; }
        
        public virtual string Name { get; set; }

        public virtual string Phone { get; set; }

        public virtual string Email { get; set; }

        public virtual string ContactMethod { get; set; }

        public virtual string NextContact { get; set; }

        public virtual IEnumerable<DateTime> ContactDates()
        {
            return DateService.ToListOfDates(NextContact, CreatedDate);
        }
    }
    }
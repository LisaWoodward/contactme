﻿using System.Collections.Generic;

using ContactMe.Core.Domain;

namespace ContactMe.Core.Services
{
    public interface IContactService
    {
        IEnumerable<Contact> ContactsByFirstContactDate();
        void SaveContact(Contact c);
    }
}
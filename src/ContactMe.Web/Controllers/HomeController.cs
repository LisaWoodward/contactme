﻿using System;
using System.Web.Mvc;

using ContactMe.Core.Repository;
using ContactMe.Web.Models;

namespace ContactMe.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IContactRepository contactRepository;

        public HomeController(IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ContactViewModel contactViewModel)
        {
            throw new NotImplementedException();
        }

    }
}
